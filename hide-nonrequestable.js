var rtacCall = setInterval(function() {
  if (window.parent.document.querySelector('.rtac-table') === null) { // keep trying until rtac shows up
  } else {
    $('.result-list-record', window.parent.document).each(function() {
      var table = $(this).find('.rtac-table tbody').text();
      var requestLinks = $(this).find('.custom-link a[title~="request"]');
      var circulating = /Circulating|Juvenile|Children|Popular|SCC-3rd Floor|New Book|Display Books|Success|ARC-DVDs-1st Floor|(Inst((ruct)?)\.Med-1st Flr)/g;
      if (table.search(circulating) === -1) {
        requestLinks.addClass('hidden');
      }
    });
  }
}, 200);
setTimeout(function() {
  clearInterval(rtacCall);
}, 8000); // stops looking for rtac after 8 seconds.